//
//  CustomTableViewController.h
//  TableViewCustomCell
//
//  Created by ja053157 on 2013/05/21.
//  Copyright (c) 2013年 Shingo Moriyasu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate>
{
    NSArray *aTableData;
}

@property (strong,nonatomic) NSMutableArray *maSearchData;
@property IBOutlet UISearchBar *mySearchBar;

@end
