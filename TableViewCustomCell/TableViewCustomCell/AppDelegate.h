//
//  AppDelegate.h
//  TableViewCustomCell
//
//  Created by ja053157 on 2013/05/21.
//  Copyright (c) 2013年 Shingo Moriyasu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
