//
//  main.m
//  TableViewCustomCell
//
//  Created by ja053157 on 2013/05/21.
//  Copyright (c) 2013年 Shingo Moriyasu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
